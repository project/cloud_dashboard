# Build cloud_dashboard
export YARN='yarn'
if [ $(echo cat /etc/issue | grep -e Debian -e Ubuntu) ]; then
  export YARN='yarnpkg'
fi

"${YARN}" react-scripts build

# Remove old files
rm -f ../js/2.chunk.js
rm -f ../js/3.chunk.js
rm -f ../js/main.chunk.js
rm -f ../js/runtime-main.js
rm -f ../css/2.chunk.css
rm -f ../css/main.chunk.css

# Copy new files from build directory
cp build/static/js/2.*.chunk.js ../js/2.chunk.js
cp build/static/js/3.*.chunk.js ../js/3.chunk.js
cp build/static/js/main.*.chunk.js ../js/main.chunk.js
cp build/static/js/runtime-main.*.js ../js/runtime-main.js
cp build/static/css/2.*.chunk.css ../css/2.chunk.css
cp build/static/css/main.*.chunk.css ../css/main.chunk.css
