import EntityInfoTemplate from 'model/EntityInfoTemplate';

const AWS_CLOUD_CARRIER_GATEWAY_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'aws_cloud',
  entityName: 'carrier_gateway',
  entityRecords: [
    {
      panelName: 'Carrier Gateway',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Carrier Gateway ID', name: 'carrier_gateway_id', type: 'default' },
        { labelName: 'VPC ID', name: 'vpc_id', type: 'default' },
        { labelName: 'State', name: 'state', type: 'default' },
        { labelName: 'AWS account ID', name: 'account_id', type: 'default' },
        { labelName: 'Created', name: 'created', type: 'datetime' },
      ]
    },
    {
      panelName: 'Tags',
      tableRecordList: ['tags'],
      keyValueRecords: [
        { labelName: 'Tags', name: 'tags', type: 'default' },
      ]
    },
    {
      panelName: 'Others',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud service provider ID', name: 'cloud_context', type: 'default' },
      ]
    },
  ]
};

export default AWS_CLOUD_CARRIER_GATEWAY_TEMPLATE;
