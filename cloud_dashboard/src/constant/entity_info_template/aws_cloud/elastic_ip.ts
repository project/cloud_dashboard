import EntityInfoTemplate from 'model/EntityInfoTemplate';

const AWS_CLOUD_ELASTIC_IP_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'aws_cloud',
  entityName: 'elastic_ip',
  entityRecords: [
    {
      panelName: 'IP Address',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Type', name: 'elastic_ip_type', type: 'default' },
        { labelName: 'Elastic IP', name: 'public_ip', type: 'default' },
        { labelName: 'Private IP Address', name: 'private_ip_address', type: 'default' },
        { labelName: 'Created', name: 'created', type: 'default' },
      ]
    },
    {
      panelName: 'Assign',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Instance ID', name: 'instance_id', type: 'default' },
        { labelName: 'Network interface ID', name: 'network_interface_id', type: 'default' },
        { labelName: 'Allocation ID', name: 'allocation_id', type: 'default' },
        { labelName: 'Association ID', name: 'association_id', type: 'default' },
        { labelName: 'Domain (Standard | VPC)', name: 'domain', type: 'default' },
        { labelName: 'Network interface owner', name: 'network_interface_owner', type: 'default' },
        { labelName: 'Network border group', name: 'network_border_group', type: 'default' },
      ]
    },
  ]
};

export default AWS_CLOUD_ELASTIC_IP_TEMPLATE;
