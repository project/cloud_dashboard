import EntityInfoTemplate from 'model/EntityInfoTemplate';

const AWS_CLOUD_IMAGE_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'aws_cloud',
  entityName: 'image',
  entityRecords: [
    {
      panelName: 'Image',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Name', name: 'name', type: 'default' },
        { labelName: 'Description', name: 'description', type: 'default' },
        { labelName: 'AMI Name', name: 'ami_name', type: 'default' },
        { labelName: 'Image ID', name: 'image_id', type: 'default' },
        { labelName: 'Owner', name: 'account_id', type: 'default' },
        { labelName: 'Source', name: 'source', type: 'default' },
        { labelName: 'Status', name: 'status', type: 'default' },
        { labelName: 'State Reason', name: 'state_reason', type: 'default' },
        { labelName: 'Created', name: 'created', type: 'datetime' },
      ]
    },
    {
      panelName: 'Launch Permission',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Visibility', name: 'visibility', type: 'boolean', value: ['Public', 'Private'] },
        { labelName: 'AWS account IDs', name: 'launch_permission_account_ids', type: 'default' },
      ]
    },
    {
      panelName: 'Type',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Platform', name: 'platform', type: 'default'},
        { labelName: 'Architecture', name: 'architecture', type: 'default' },
        { labelName: 'Virtualization Type', name: 'virtualization_type', type: 'default' },
        { labelName: 'Product Code', name: 'product_code', type: 'default' },
        { labelName: 'Image Type', name: 'image_type', type: 'default'},
      ]
    },
    {
      panelName: 'Device',
      tableRecordList: ['block_device_mappings'],
      keyValueRecords: [
        { labelName: 'Root Device Name', name: 'root_device_name', type: 'default' },
        { labelName: 'Root Device Type', name: 'root_device_type', type: 'default' },
        { labelName: 'Kernel ID', name: 'kernel_id', type: 'default' },
        { labelName: 'RAM Disk ID', name: 'ramdisk_id', type: 'default' },
        { labelName: 'Block Device Mappings', name: 'block_device_mappings', type: 'default' },
      ]
    },
  ]
};

export default AWS_CLOUD_IMAGE_TEMPLATE;
