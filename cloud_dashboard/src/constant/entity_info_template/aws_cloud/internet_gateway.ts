import EntityInfoTemplate from 'model/EntityInfoTemplate';

const AWS_CLOUD_INTERNET_GATEWAY_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'aws_cloud',
  entityName: 'internet_gateway',
  entityRecords: [
    {
      panelName: 'Internet Gateway',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Internet Gateway ID', name: 'internet_gateway_id', type: 'default' },
        { labelName: 'VPC ID', name: 'vpc_id', type: 'default' },
        { labelName: 'State', name: 'state', type: 'default' },
        { labelName: 'AWS account ID', name: 'account_id', type: 'default' },
        { labelName: 'Created', name: 'created', type: 'datetime' },
      ]
    },
    {
      panelName: 'Tags',
      tableRecordList: ['tags'],
      keyValueRecords: [
        { labelName: 'Tags', name: 'tags', type: 'default' },
      ]
    },
  ]
};

export default AWS_CLOUD_INTERNET_GATEWAY_TEMPLATE;
