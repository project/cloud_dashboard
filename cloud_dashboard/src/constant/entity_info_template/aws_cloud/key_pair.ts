import EntityInfoTemplate from 'model/EntityInfoTemplate';

const AWS_CLOUD_KEY_PAIR_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'aws_cloud',
  entityName: 'key_pair',
  entityRecords: [
    {
      panelName: 'Key pair',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Key pair name', name: 'key_pair_name', type: 'default' },
        { labelName: 'Key pair ID', name: 'key_pair_id', type: 'default' },
        { labelName: 'Fingerprint', name: 'key_fingerprint', type: 'default' },
        { labelName: 'Created', name: 'created', type: 'datetime' },
      ]
    }
  ]

};

export default AWS_CLOUD_KEY_PAIR_TEMPLATE;
