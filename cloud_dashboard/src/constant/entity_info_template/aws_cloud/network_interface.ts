import EntityInfoTemplate from 'model/EntityInfoTemplate';

const AWS_CLOUD_NETWORK_INTERFACE_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'aws_cloud',
  entityName: 'network_interface',
  entityRecords: [
    {
      panelName: 'Network interface',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Description', name: 'description', type: 'default' },
        { labelName: 'Network interface ID', name: 'network_interface_id', type: 'default' },
        { labelName: 'Instance ID', name: 'instance_id', type: 'default' },
        { labelName: 'Allocation ID', name: 'allocation_id', type: 'default' },
        { labelName: 'Mac Address', name: 'mac_address', type: 'default' },
        { labelName: 'Device Index', name: 'device_index', type: 'number' },
        { labelName: 'Status', name: 'status', type: 'default' },
        { labelName: 'Delete on Termination', name: 'delete_on_termination', type: 'default' },
        { labelName: 'Created', name: 'created', type: 'datetime' },
      ]
    },
    {
      panelName: 'Network',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Security Groups', name: 'security_groups', type: 'default' },
        { labelName: 'VPC ID', name: 'vpc_id', type: 'default' },
        { labelName: 'CIDR Block', name: 'cidr_block', type: 'default' },
        { labelName: 'Subnet ID', name: 'subnet_id', type: 'default' },
        { labelName: 'Public IPs', name: 'public_ips', type: 'default' },
        { labelName: 'Primary Private IP', name: 'primary_private_ip', type: 'default' },
        { labelName: 'Secondary Private IPs', name: 'secondary_private_ips', type: 'default' },
        { labelName: 'Private DNS', name: 'private_dns', type: 'default' },
      ]
    },
    {
      panelName: 'Attachment',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Attachment ID', name: 'attachment_id', type: 'default' },
        { labelName: 'Attachment Owner', name: 'attachment_owner', type: 'default' },
        { labelName: 'Attachment Status', name: 'attachment_status', type: 'default' },
      ]
    },
    {
      panelName: 'Owner',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'AWS account ID', name: 'account_id', type: 'default' },
      ]
    },
  ]
};

export default AWS_CLOUD_NETWORK_INTERFACE_TEMPLATE;
