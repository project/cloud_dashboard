import EntityInfoTemplate from 'model/EntityInfoTemplate';
import EntityColumn from 'model/EntityColumn';

// Template for displaying the Launch template in K8s.
const COLUMN_RULES: EntityColumn[] = [
  { labelName: 'IP Protocol', 'name': 'ip_protocol', type: 'conditions', value: ['-1', 'All Traffic']},
  { labelName: 'From Port', 'name': 'from_port', type: 'default' },
  { labelName: 'To Port', 'name': 'to_port', type: 'default' },
  { labelName: 'CIDR IP', 'name': 'cidr_ip', type: 'default' },
  { labelName: 'CIDR IP V6 ', 'name': 'cidr_ip_v6', type: 'default' },
  { labelName: 'Prefix List ID', 'name': 'prefix_list_id', type: 'default' },
  { labelName: 'Group ID', 'name': 'group_id', type: 'default' },
  { labelName: 'Group Name', 'name': 'group_name', type: 'default' },
  { labelName: 'Peering Status', 'name': 'peering_status', type: 'default' },
  { labelName: 'Group User ID', 'name': 'user_id', type: 'default' },
  { labelName: 'VPC ID', 'name': 'vpc_id', type: 'default' },
  { labelName: 'Peering Connection ID', 'name': 'peering_connection_id', type: 'default' },
  { labelName: 'Description', 'name': 'description', type: 'default' },
];

const AWS_CLOUD_SECURITY_GROUP_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'aws_cloud',
  entityName: 'security_group',
  entityRecords: [
    {
      panelName: 'Security group',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Security group name', name: 'group_name', type: 'default' },
        { labelName: 'ID', name: 'group_id', type: 'default' },
        { labelName: 'Description', name: 'description', type: 'default' },
        { labelName: 'VPC ID', name: 'vpc_id', type: 'default' },
        { labelName: 'Created', name: 'created', type: 'datetime' },
      ]
    },
    {
      panelName: 'Rules',
      tableRecordList: ['outbound_permission'],
      keyValueRecords: [
        { labelName: 'Inbound Rules', name: 'ip_permission', type: 'custom-table', column: COLUMN_RULES },
        { labelName: 'Outbound Rules', name: 'outbound_permission', type: 'custom-table', column: COLUMN_RULES },
      ]
    },
  ]
};

export default AWS_CLOUD_SECURITY_GROUP_TEMPLATE;
