import EntityInfoTemplate from 'model/EntityInfoTemplate';

const AWS_CLOUD_SNAPSHOT_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'aws_cloud',
  entityName: 'snapshot',
  entityRecords: [
    {
      panelName: 'Snapshot',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Description', name: 'description', type: 'default' },
        { labelName: 'Snapshot ID', name: 'snapshot_id', type: 'default' },
        { labelName: 'Volume ID', name: 'volume_id', type: 'default' },
        { labelName: 'Size', name: 'size', type: 'number' },
        { labelName: 'Status', name: 'status', type: 'default' },
        { labelName: 'Progress', name: 'progress', type: 'default' },
        { labelName: 'Encrypted', name: 'encrypted', type: 'default' },
        { labelName: 'Created', name: 'created', type: 'datetime' },
      ]
    },
  ]
};

export default AWS_CLOUD_SNAPSHOT_TEMPLATE;
