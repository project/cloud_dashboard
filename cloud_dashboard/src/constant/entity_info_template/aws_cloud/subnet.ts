import EntityInfoTemplate from 'model/EntityInfoTemplate';

const AWS_CLOUD_SUBNET_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'aws_cloud',
  entityName: 'subnet',
  entityRecords: [
    {
      panelName: 'Subnet',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'CIDR Block', name: 'cidr_block', type: 'default' },
        { labelName: 'Subnet ID', name: 'subnet_id', type: 'default' },
        { labelName: 'VPC', name: 'vpc_id', type: 'default' },
        { labelName: 'State', name: 'state', type: 'default' },
        { labelName: 'AWS account ID', name: 'account_id', type: 'default' },
        {
          labelName: 'Availability Zone', name: 'availability_zone', type: 'join', info: {
            entityTypeId: 'aws_cloud_instance',
            keyColumn: 'availability_zone',
            valueColumn: 'name',
          }
        },
        { labelName: 'Opt-In Status', name: 'opt_in_status', type: 'default' },
        { labelName: 'Region Name', name: 'region_name', type: 'default' },
        { labelName: 'Zone Name', name: 'zone_name', type: 'default' },
        { labelName: 'Zone ID', name: 'zone_id', type: 'default' },
        { labelName: 'Group Name', name: 'group_name', type: 'default' },
        { labelName: 'Network border group', name: 'network_border_group', type: 'default' },
        { labelName: 'Zone Type', name: 'zone_type', type: 'default' },
        { labelName: 'Created', name: 'created', type: 'datetime' },
      ]
    },
    {
      panelName: 'Tags',
      tableRecordList: ['tags'],
      keyValueRecords: [
        { labelName: 'Tags', name: 'tags', type: 'default' },
      ]
    },
  ]
};

export default AWS_CLOUD_SUBNET_TEMPLATE;
