import EntityInfoTemplate from 'model/EntityInfoTemplate';

const AWS_CLOUD_TRANSIT_GATEWAY_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'aws_cloud',
  entityName: 'transit_gateway',
  entityRecords: [
    {
      panelName: 'Transit Gateway',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Name', name: 'name', type: 'default' },
        { labelName: 'Transit Gateway ID', name: 'transit_gateway_id', type: 'default' },
        { labelName: 'State', name: 'state', type: 'default' },
        { labelName: 'AWS account ID', name: 'account_id', type: 'default' },
        { labelName: 'Amazon Side ASN', name: 'amazon_side_asn', type: 'default' },
        { labelName: 'Association Default Route Table ID', name: 'association_default_route_table_id', type: 'default' },
        { labelName: 'Auto Accept Shared Attachments', name: 'auto_accept_shared_attachments', type: 'boolean', value: ['enable', 'disable'] },
        { labelName: 'Default Route Table Association', name: 'default_route_table_association', type: 'boolean', value: ['enable', 'disable'] },
        { labelName: 'Default Route Table Propagation', name: 'default_route_table_propagation', type: 'boolean', value: ['enable', 'disable'] },
        { labelName: 'DNS Support', name: 'dns_support', type: 'boolean', value: ['enable', 'disable'] },
        { labelName: 'Multicast Support', name: 'multicast_support', type: 'boolean', value: ['enable', 'disable'] },
        { labelName: 'Propagation Default Route Table ID', name: 'propagation_default_route_table_id', type: 'default' },
        { labelName: 'VPN ECMP Support', name: 'vpn_ecmp_support', type: 'boolean', value: ['enable', 'disable'] },
        { labelName: 'Created', name: 'created', type: 'datetime' },
      ]
    },
    {
      panelName: 'Tags',
      tableRecordList: ['tags'],
      keyValueRecords: [
        { labelName: 'Tags', name: 'tags', type: 'default' },
      ]
    },
    {
      panelName: 'Others',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud service provider ID', name: 'cloud_context', type: 'default' },
      ]
    },
  ]

};

export default AWS_CLOUD_TRANSIT_GATEWAY_TEMPLATE;
