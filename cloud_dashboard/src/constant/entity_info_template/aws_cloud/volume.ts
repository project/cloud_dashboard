import EntityInfoTemplate from 'model/EntityInfoTemplate';

const AWS_CLOUD_VOLUME_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'aws_cloud',
  entityName: 'volume',
  entityRecords: [
    {
      panelName: 'Volume',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Volume ID', name: 'volume_id', type: 'default' },
        { labelName: 'Instance ID', name: 'attachment_information', type: 'default' },
        { labelName: 'Snapshot ID', name: 'snapshot_id', type: 'default' },
        { labelName: 'Size (GB)', name: 'size', type: 'number' },
        { labelName: 'Volume type', name: 'volume_type', type: 'default' },
        { labelName: 'IOPS', name: 'iops', type: 'number' },
        { labelName: 'Availability Zone', name: 'availability_zone', type: 'default' },
        { labelName: 'Encrypted', name: 'encrypted', type: 'boolean', value: [
          'On', 'Off'
        ] },
        { labelName: 'Status', name: 'state', type: 'default' },
        { labelName: 'Created', name: 'created', type: 'datetime' },
      ]
    },
  ]
};

export default AWS_CLOUD_VOLUME_TEMPLATE;
