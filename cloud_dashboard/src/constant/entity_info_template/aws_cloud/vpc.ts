import EntityInfoTemplate from 'model/EntityInfoTemplate';

// Template for displaying detailed information about entities in AWS Cloud.
const AWS_CLOUD_VPC_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'aws_cloud',
  entityName: 'vpc',
  entityRecords: [
    {
      panelName: 'VPC',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'VPC ID', name: 'vpc_id', type: 'default' },
        { labelName: 'State', name: 'state', type: 'default' },
        { labelName: 'DHCP Options ID', name: 'dhcp_options_id', type: 'default' },
        { labelName: 'Instance Tenancy', name: 'instance_tenancy', type: 'default' },
        { labelName: 'Default VPC', name: 'is_default', type: 'boolean', value: [
          'Yes', 'No'
        ] },
        { labelName: 'AWS account ID', name: 'account_id', type: 'default' },
        { labelName: 'Created', name: 'created', type: 'datetime' },
      ],
    },
    {
      panelName: 'Flow logs',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Flow log', name: 'flow_log', type: 'boolean', value: [
          'On', 'Off'
        ] },
      ]
    },
    {
      panelName: 'CIDR Blocks',
      tableRecordList: ['cidr_blocks'],
      keyValueRecords: [
        { labelName: 'IPv4 CIDR', name: 'cidr_blocks', type: 'default' },
      ],
    },
    {
      panelName: 'IPv6 CIDR Blocks',
      tableRecordList: ['cidr_blocks'],
      keyValueRecords: [
        { labelName: 'IPv6 CIDR', name: 'ipv6_cidr_blocks', type: 'default' },
      ],
    },
    {
      panelName: 'Tags',
      tableRecordList: ['tags'],
      keyValueRecords: [
        { labelName: 'Tags', name: 'tags', type: 'default' },
      ],
    }
  ],
}

export default AWS_CLOUD_VPC_TEMPLATE;
