import EntityInfoTemplate from 'model/EntityInfoTemplate';

const AWS_CLOUD_VPC_PEERING_CONNECTION_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'aws_cloud',
  entityName: 'vpc_peering_connection',
  entityRecords: [
    {
      panelName: 'VPC Peering Connection',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'VPC Peering Connection ID', name: 'vpc_peering_connection_id', type: 'default' },
        { labelName: 'Status Code', name: 'status_code', type: 'default' },
        { labelName: 'Status Message', name: 'status_message', type: 'default' },
        { labelName: 'Expiration Time', name: 'expiration_time', type: 'datetime' },
        { labelName: 'Created', name: 'created', type: 'datetime' },
      ]
    },
    {
      panelName: 'Requester',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Requester VPC ID', name: 'requester_vpc_id', type: 'default' },
        { labelName: 'Requester CIDR Block', name: 'requester_cidr_block', type: 'default' },
        { labelName: 'Requester AWS account ID', name: 'requester_account_id', type: 'default' },
        { labelName: 'Requester Region', name: 'requester_region', type: 'default' },
      ]
    },
    {
      panelName: 'Accepter',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Accepter VPC ID', name: 'accepter_vpc_id', type: 'default' },
        { labelName: 'Accepter CIDR Block', name: 'accepter_cidr_block', type: 'default' },
        { labelName: 'Accepter AWS account ID', name: 'accepter_account_id', type: 'default' },
        { labelName: 'Accepter region', name: 'accepter_region', type: 'default' },
      ]
    },
    {
      panelName: 'Tags',
      tableRecordList: ['tags'],
      keyValueRecords: [
        { labelName: 'Tags', name: 'tags', type: 'default' },
      ]
    },
    {
      panelName: 'Others',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud service provider ID', name: 'cloud_context', type: 'default' },
      ]
    },
  ]
};

export default AWS_CLOUD_VPC_PEERING_CONNECTION_TEMPLATE;
