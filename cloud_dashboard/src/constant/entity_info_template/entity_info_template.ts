import AWS_CLOUD_INSTANCE_TEMPLATE from 'constant/entity_info_template/aws_cloud/instance';
import AWS_CLOUD_IMAGE_TEMPLATE from "constant/entity_info_template/aws_cloud/image";
import AWS_CLOUD_SECURITY_GROUP_TEMPLATE from "constant/entity_info_template/aws_cloud/security_group";
import AWS_CLOUD_ELASTIC_IP_TEMPLATE from "constant/entity_info_template/aws_cloud/elastic_ip";
import AWS_CLOUD_KEY_PAIR_TEMPLATE from 'constant/entity_info_template/aws_cloud/key_pair';
import AWS_CLOUD_VOLUME_TEMPLATE from 'constant/entity_info_template/aws_cloud/volume';
import AWS_CLOUD_SNAPSHOT_TEMPLATE from 'constant/entity_info_template/aws_cloud/snapshot';
import AWS_CLOUD_NETWORK_INTERFACE_TEMPLATE from 'constant/entity_info_template/aws_cloud/network_interface';
import AWS_CLOUD_VPC_TEMPLATE from 'constant/entity_info_template/aws_cloud/vpc';
import AWS_CLOUD_TRANSIT_GATEWAY_TEMPLATE from 'constant/entity_info_template/aws_cloud/transit_gateway';
import AWS_CLOUD_SUBNET_TEMPLATE from 'constant/entity_info_template/aws_cloud/subnet';
import AWS_CLOUD_VPC_PEERING_CONNECTION_TEMPLATE from 'constant/entity_info_template/aws_cloud/vpc_peering_connection';
import AWS_CLOUD_INTERNET_GATEWAY_TEMPLATE from 'constant/entity_info_template/aws_cloud/internet_gateway';
import AWS_CLOUD_CARRIER_GATEWAY_TEMPLATE from 'constant/entity_info_template/aws_cloud/carrier_gateway';
import K8S_NODE_TEMPLATE from 'constant/entity_info_template/k8s/node';
import K8S_POD_TEMPLATE from 'constant/entity_info_template/k8s/pod';
import K8S_DEPLOYMENT_TEMPLATE from 'constant/entity_info_template/k8s/deployment';
import K8S_NAMESPACE_TEMPLATE from 'constant/entity_info_template/k8s/namespace';
import K8S_CRONJOB_TEMPLATE from 'constant/entity_info_template/k8s/cronjob';
import K8S_REPLICA_SET_TEMPLATE from 'constant/entity_info_template/k8s/replica_set';
import K8S_NETWORK_POLICY_TEMPLATE from 'constant/entity_info_template/k8s/network_policy';
import K8S_RESOURCE_QUOTA_TEMPLATE from 'constant/entity_info_template/k8s/resource_quota';
import K8S_PRIORITY_CLASS_TEMPLATE from 'constant/entity_info_template/k8s/priority_class';
import K8S_CONFIG_MAP_TEMPLATE from 'constant/entity_info_template/k8s/config_map';
import K8S_SECRET_TEMPLATE from 'constant/entity_info_template/k8s/secret';
import K8S_ROLE_TEMPLATE from 'constant/entity_info_template/k8s/role';
import K8S_ROLE_BINDING_TEMPLATE from 'constant/entity_info_template/k8s/role_binding';
import K8S_CLUSTER_ROLE_TEMPLATE from 'constant/entity_info_template/k8s/cluster_role';
import K8S_CLUSTER_ROLE_BINDING_TEMPLATE from 'constant/entity_info_template/k8s/cluster_role_binding';
import K8S_PERSISTENT_VOLUME_TEMPLATE from 'constant/entity_info_template/k8s/persistent_volume';
import K8S_PERSISTENT_VOLUME_CLAIM_TEMPLATE from 'constant/entity_info_template/k8s/persistent_volume_claim';
import K8S_STORAGE_CLASS_TEMPLATE from 'constant/entity_info_template/k8s/storage_class';
import K8S_JOB_TEMPLATE from 'constant/entity_info_template/k8s/job';
import K8S_SERVICE_TEMPLATE from 'constant/entity_info_template/k8s/service';
import K8S_ENDPOINT_TEMPLATE from 'constant/entity_info_template/k8s/endpoint';
import K8S_STATEFUL_SET_TEMPLATE from 'constant/entity_info_template/k8s/stateful_set';
import K8S_LIMIT_RANGE_TEMPLATE from 'constant/entity_info_template/k8s/limit_range';
import K8S_INGRESS_TEMPLATE from 'constant/entity_info_template/k8s/ingress';
import K8S_DAEMON_SET_TEMPLATE from 'constant/entity_info_template/k8s/daemon_set';
import K8S_SERVICE_ACCOUNT_TEMPLATE from 'constant/entity_info_template/k8s/service_account';
import K8S_HORIZONTAL_POD_AUTOSCALER_TEMPLATE from 'constant/entity_info_template/k8s/horizontal_pod_autoscaler';
import K8S_API_SERVICE_TEMPLATE from 'constant/entity_info_template/k8s/api_service';
import K8S_SCHEDULE_TEMPLATE from 'constant/entity_info_template/k8s/schedule';

const ENTITY_INFO_LIST = [
  AWS_CLOUD_INSTANCE_TEMPLATE,
  AWS_CLOUD_IMAGE_TEMPLATE,
  AWS_CLOUD_SECURITY_GROUP_TEMPLATE,
  AWS_CLOUD_ELASTIC_IP_TEMPLATE,
  AWS_CLOUD_KEY_PAIR_TEMPLATE,
  AWS_CLOUD_VOLUME_TEMPLATE,
  AWS_CLOUD_SNAPSHOT_TEMPLATE,
  AWS_CLOUD_NETWORK_INTERFACE_TEMPLATE,
  AWS_CLOUD_VPC_TEMPLATE,
  AWS_CLOUD_TRANSIT_GATEWAY_TEMPLATE,
  AWS_CLOUD_SUBNET_TEMPLATE,
  AWS_CLOUD_VPC_PEERING_CONNECTION_TEMPLATE,
  AWS_CLOUD_INTERNET_GATEWAY_TEMPLATE,
  AWS_CLOUD_CARRIER_GATEWAY_TEMPLATE,
  K8S_NODE_TEMPLATE,
  K8S_POD_TEMPLATE,
  K8S_DEPLOYMENT_TEMPLATE,
  K8S_NAMESPACE_TEMPLATE,
  K8S_CRONJOB_TEMPLATE,
  K8S_REPLICA_SET_TEMPLATE,
  K8S_NETWORK_POLICY_TEMPLATE,
  K8S_RESOURCE_QUOTA_TEMPLATE,
  K8S_PRIORITY_CLASS_TEMPLATE,
  K8S_CONFIG_MAP_TEMPLATE,
  K8S_SECRET_TEMPLATE,
  K8S_ROLE_TEMPLATE,
  K8S_ROLE_BINDING_TEMPLATE,
  K8S_CLUSTER_ROLE_TEMPLATE,
  K8S_CLUSTER_ROLE_BINDING_TEMPLATE,
  K8S_PERSISTENT_VOLUME_TEMPLATE,
  K8S_PERSISTENT_VOLUME_CLAIM_TEMPLATE,
  K8S_STORAGE_CLASS_TEMPLATE,
  K8S_JOB_TEMPLATE,
  K8S_SERVICE_TEMPLATE,
  K8S_ENDPOINT_TEMPLATE,
  K8S_STATEFUL_SET_TEMPLATE,
  K8S_LIMIT_RANGE_TEMPLATE,
  K8S_INGRESS_TEMPLATE,
  K8S_DAEMON_SET_TEMPLATE,
  K8S_SERVICE_ACCOUNT_TEMPLATE,
  K8S_HORIZONTAL_POD_AUTOSCALER_TEMPLATE,
  K8S_API_SERVICE_TEMPLATE,
  K8S_SCHEDULE_TEMPLATE,
];

export const UPPERCASE_WORD_SET = new Set([
  'cidr', 'id', 'ip', 'v6', 'yaml'
]);

export default ENTITY_INFO_LIST;
