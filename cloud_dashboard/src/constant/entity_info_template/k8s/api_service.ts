import EntityInfoTemplate from 'model/EntityInfoTemplate';

const K8S_API_SERVICE_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'k8s',
  entityName: 'api_service',
  entityRecords: [
    {
      panelName: 'API Service',
      tableRecordList: ['service', 'labels'],
      keyValueRecords: [
        { labelName: 'Name', name: 'name', type: 'default' },
        { labelName: 'Labels', name: 'labels', type: 'default' },
        { labelName: 'Group Priority Minimum', name: 'group_priority_minimum', type: 'number' },
        { labelName: 'Service', name: 'service', type: 'default' },
        { labelName: 'Version Priority', name: 'version_priority', type: 'number' },
        { labelName: 'Group', name: 'group', type: 'default' },
        { labelName: 'InsecureSkipTlsVerify', name: 'insecure_skip_tls_verify', type: 'boolean', value: ['True', 'False'] },
        { labelName: 'Version', name: 'version', type: 'default' },
      ]
    },
    {
      panelName: 'Status',
      tableRecordList: ['conditions'],
      keyValueRecords: [
        {labelName: 'Conditions', name: 'conditions', type: 'default' },
      ]
    },
    {
      panelName: 'Detail',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Detail', name: 'detail', type: 'default' },
      ]
    },
    {
      panelName: 'Other',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud Service Provider ID', name: 'cloud_context', type: 'default' },
      ]
    },
  ]
}

export default K8S_API_SERVICE_TEMPLATE;
