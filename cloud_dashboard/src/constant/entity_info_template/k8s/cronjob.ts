import EntityInfoTemplate from 'model/EntityInfoTemplate';

const K8S_CRONJOB_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'k8s',
  entityName: 'cron_job',
  entityRecords: [
    {
      panelName: 'CronJob',
      tableRecordList: ['annotations'],
      keyValueRecords: [
        { labelName: 'Name', name: 'name', type: 'default' },
        { labelName: 'Annotations', name: 'annotations', type: 'default' },
        { labelName: 'Namespace', name: 'namespace', type: 'default' },
        { labelName: 'Schedule', name: 'schedule', type: 'default' },
        { labelName: 'Active', name: 'active', type: 'number' },
        { labelName: 'Suspend', name: 'suspend', type: 'boolean',  value: ['true', 'false'] },
        { labelName: 'Last Schedule Time', name: 'last_schedule_time', type: 'datetime' },
        { labelName: 'Concurrency Policy', name: 'concurrency_policy', type: 'default' },
        { labelName: 'Starting Deadline Seconds', name: 'starting_deadline_seconds', type: 'number' },
      ]
    },
    {
      panelName: 'Detail',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Detail', name: 'detail', type: 'default' },
      ]
    },
    {
      panelName: 'Other',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud Service Provider ID', name: 'cloud_context', type: 'default' },
      ]
    },
  ]
}

export default K8S_CRONJOB_TEMPLATE;