import EntityInfoTemplate from 'model/EntityInfoTemplate';

const K8S_DAEMON_SET_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'k8s',
  entityName: 'daemon_set',
  entityRecords: [
    {
      panelName: 'Ingress',
      tableRecordList: ['annotations', 'labels'],
      keyValueRecords: [
        { labelName: 'Name', name: 'name', type: 'default' },
        { labelName: 'Labels', name: 'labels', type: 'default' },
        { labelName: 'Annotations', name: 'annotations', type: 'default' },
        { labelName: 'Namespace', name: 'namespace', type: 'default' },
        { labelName: 'CPU (Request)', name: 'cpu_request', type: 'number' },
        { labelName: 'CPU (Limit)', name: 'cpu_limit', type: 'number' },
        { labelName: 'Memory (Request)', name: 'memory_request', type: 'number' },
        { labelName: 'Memory (Limit)', name: 'memory_limit', type: 'number' },
      ]
    },
    {
      panelName: 'Detail',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Detail', name: 'detail', type: 'default' },
      ]
    },
    {
      panelName: 'Other',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud Service Provider ID', name: 'cloud_context', type: 'default' },
      ]
    },
  ]
}

export default K8S_DAEMON_SET_TEMPLATE;

