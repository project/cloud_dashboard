import EntityInfoTemplate from 'model/EntityInfoTemplate';

const K8S_DEPLOYMENT_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'k8s',
  entityName: 'deployment',
  entityRecords: [
    {
      panelName: 'Deployment',
      tableRecordList: ['labels', 'annotations'],
      keyValueRecords: [
        { labelName: 'Name', name: 'name', type: 'default' },
        { labelName: 'Labels', name: 'labels', type: 'default' },
        { labelName: 'Annotations', name: 'annotations', type: 'default' },
        { labelName: 'Namespace', name: 'namespace', type: 'default' },
        { labelName: 'Strategy', name: 'strategy', type: 'default' },
        { labelName: 'Minimum ready seconds', name: 'min_ready_seconds', type: 'number' },
        { labelName: 'Revision History Limit', name: 'revision_history_limit', type: 'number' },
      ]
    },
    {
      panelName: 'Status',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Available Replicas', name: 'available_replicas', type: 'number' },
        { labelName: 'Collision Count', name: 'collision_count', type: 'number' },
        { labelName: 'Observed Generation', name: 'observed_generation', type: 'number' },
        { labelName: 'Ready Replicas', name: 'ready_replicas', type: 'number' },
        { labelName: 'Replicas', name: 'replicas', type: 'number' },
        { labelName: 'Unavailable Replicas', name: 'unavailable_replicas', type: 'number' },
        { labelName: 'Updated Replicas', name: 'updated_replicas', type: 'number' },
      ]
    },
    {
      panelName: 'Detail',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Detail', name: 'detail', type: 'default' },
      ]
    },
    {
      panelName: 'Other',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud Service Provider ID', name: 'cloud_context', type: 'default' },
      ]
    },
  ]
}

export default K8S_DEPLOYMENT_TEMPLATE;
