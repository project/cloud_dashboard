import EntityInfoTemplate from 'model/EntityInfoTemplate';

const K8S_ENDPOINT_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'k8s',
  entityName: 'endpoint',
  entityRecords: [
    {
      panelName: 'Endpoint',
      tableRecordList: ['addresses'],
      keyValueRecords: [
        { labelName: 'Name', name: 'name', type: 'default' },
        { labelName: 'Namespace', name: 'namespace', type: 'default' },
        { labelName: 'Addresses', name: 'addresses', type: 'default' },
      ]
    },
    {
      panelName: 'Detail',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Detail', name: 'detail', type: 'default' },
      ]
    },
    {
      panelName: 'Other',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud Service Provider ID', name: 'cloud_context', type: 'default' },
      ]
    },
  ]
}

export default K8S_ENDPOINT_TEMPLATE;