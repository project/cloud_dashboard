import EntityInfoTemplate from 'model/EntityInfoTemplate';

const K8S_HORIZONTAL_POD_AUTOSCALER_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'k8s',
  entityName: 'horizontal_pod_autoscaler',
  entityRecords: [
    {
      panelName: 'Horizontal Pod Autoscaler',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Name', name: 'name', type: 'default' },
        { labelName: 'Namespace', name: 'namespace', type: 'default' },
        { labelName: 'Scale Target', name: 'scale_target', type: 'default' },
        { labelName: 'Target CPU Utilization', name: 'target_cpu_utilization', type: 'number' },
      ]
    },
    {
      panelName: 'Status',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Minimum Replicas', name: 'minimum_replicas', type: 'number' },
        { labelName: 'Maximum Replicas', name: 'maximum_replicas', type: 'number' },
        { labelName: 'Deployment Pods', name: 'deployment_pods', type: 'number' },
        { labelName: 'Resource CPU on Pods (%)', name: 'resource_cpu_on_pods', type: 'number' },
      ]
    },
    {
      panelName: 'Detail',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Detail', name: 'detail', type: 'default' },
      ]
    },
    {
      panelName: 'Other',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud Service Provider ID', name: 'cloud_context', type: 'default' },
      ]
    },
  ]
}

export default K8S_HORIZONTAL_POD_AUTOSCALER_TEMPLATE;
