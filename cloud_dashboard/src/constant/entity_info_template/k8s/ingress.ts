import EntityInfoTemplate from 'model/EntityInfoTemplate';

const K8S_INGRESS_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'k8s',
  entityName: 'ingress',
  entityRecords: [
    {
      panelName: 'Ingress',
      tableRecordList: ['annotations', 'rules'],
      keyValueRecords: [
        { labelName: 'Name', name: 'name', type: 'default' },
        { labelName: 'Annotations', name: 'annotations', type: 'default' },
        { labelName: 'Namespace', name: 'namespace', type: 'default' },
        { labelName: 'Backend', name: 'backend', type: 'key-value' },
        { labelName: 'Rules', name: 'rules', type: 'default' },
        { labelName: 'TLS', name: 'tls', type: 'key-value' },
        { labelName: 'Load Balancer', name: 'load_balancer', type: 'key-value' },
      ]
    },
    {
      panelName: 'Detail',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Detail', name: 'detail', type: 'default' },
      ]
    },
    {
      panelName: 'Other',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud Service Provider ID', name: 'cloud_context', type: 'default' },
      ]
    },
  ]
}

export default K8S_INGRESS_TEMPLATE;
