import EntityInfoTemplate from 'model/EntityInfoTemplate';

const K8S_JOB_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'k8s',
  entityName: 'job',
  entityRecords: [
    {
      panelName: 'Job',
      tableRecordList: ['labels', 'annotations'],
      keyValueRecords: [
        { labelName: 'Name', name: 'name', type: 'default' },
        { labelName: 'Labels', name: 'labels', type: 'default' },
        { labelName: 'Annotations', name: 'annotations', type: 'default' },
        { labelName: 'Namespace', name: 'namespace', type: 'default' },
        { labelName: 'Image', name: 'image', type: 'default' },
        { labelName: 'Completions', name: 'completions', type: 'number' },
        { labelName: 'Parallelism', name: 'parallelism', type: 'number' },
      ]
    },
    { 
      panelName: 'Pod Status',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Active', name: 'active', type: 'number' },
        { labelName: 'Failed', name: 'failed', type: 'number' },
        { labelName: 'Succeeded', name: 'succeeded', type: 'number' },
      ]
    },
    {
      panelName: 'Detail',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Detail', name: 'detail', type: 'default' },
      ]
    },
    {
      panelName: 'Other',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud Service Provider ID', name: 'cloud_context', type: 'default' },
      ]
    },
  ]
}

export default K8S_JOB_TEMPLATE;

