import EntityInfoTemplate from 'model/EntityInfoTemplate';

// Template for displaying detailed information about entities in K8s.
const K8S_NETWORK_POLICY_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'k8s',
  entityName: 'network_policy',
  entityRecords: [
    {
      panelName: 'Network Policy',
      tableRecordList: ['labels', 'annotations'],
      keyValueRecords: [
        { labelName: 'Name', name: 'name', type: 'default' },
        { labelName: 'Namespace', name: 'namespace', type: 'default' },
        { labelName: 'Created', name: 'created', type: 'datetime' },
        { labelName: 'Egress', name: 'egress', type: 'default' },
        { labelName: 'Ingress', name: 'ingress', type: 'default' },
        { labelName: 'Pod selector', name: 'pod_selector', type: 'default' },
        { labelName: 'Policy types', name: 'policy_types', type: 'default' },
        { labelName: 'Labels', name: 'labels', type: 'default' },
        { labelName: 'Annotations', name: 'annotations', type: 'default' },
      ]
    },
    {
      panelName: 'Detail',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Detail', name: 'detail', type: 'default' },
        { labelName: 'Creation YAML', name: 'creation_yaml', type: 'default' },
      ]
    },
    {
      panelName: 'Other',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud Service Provider ID', name: 'cloud_context', type: 'default' },
      ]
    },
  ]
};

export default K8S_NETWORK_POLICY_TEMPLATE;
