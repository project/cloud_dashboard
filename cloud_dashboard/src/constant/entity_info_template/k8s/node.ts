import EntityInfoTemplate from 'model/EntityInfoTemplate';

// Template for displaying detailed information about entities in K8s.
const K8S_NODE_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'k8s',
  entityName: 'node',
  entityRecords: [
    {
      panelName: 'Metrics',
      tableRecordList: [],
      keyValueRecords: [
        {
          labelName: '', name: '', type: 'metrics', column: [
            { title: 'CPU Usage', yLabel: 'CPU (Cores)', name: 'cpu', type: 'default' },
            { title: 'Memory Usage', yLabel: 'Memory (Bytes)', name: 'memory', type: 'memory' }
          ]
        },
      ]
    },
    {
      panelName: 'Node',
      tableRecordList: ['labels', 'annotations', 'addresses'],
      keyValueRecords: [
        { labelName: 'Name', name: 'name', type: 'default' },
        { labelName: 'Labels', name: 'labels', type: 'default' },
        { labelName: 'Annotations', name: 'annotations', type: 'default' },
        { labelName: 'Status', name: 'status', type: 'default' },
        { labelName: 'Addresses', name: 'addresses', type: 'default' },
        { labelName: 'Pod CIDR', name: 'pod_cidr', type: 'default' },
        { labelName: 'Provider ID', name: 'provider_id', type: 'default' },
        { labelName: 'Unschedulable', name: 'unschedulable', type: 'boolean', value: [
          'On', 'Off'
        ] },
        { labelName: 'Created', name: 'created', type: 'datetime' },
      ]
    },
    {
      panelName: 'System Info',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud service provider ID', name: 'machine_id', type: 'default' },
        { labelName: 'System UUID', name: 'system_uuid', type: 'default' },
        { labelName: 'Boot ID', name: 'boot_id', type: 'default' },
        { labelName: 'Kernel Version', name: 'kernel_version', type: 'default' },
        { labelName: 'OS Image', name: 'os_image', type: 'default' },
        { labelName: 'Container Runtime Version', name: 'container_runtime_version', type: 'default' },
        { labelName: 'Kubelet Version', name: 'kubelet_version', type: 'default' },
        { labelName: 'KubeProxy Version', name: 'kube_proxy_version', type: 'default' },
        { labelName: 'Operating System', name: 'operating_system', type: 'default' },
        { labelName: 'Architecture', name: 'architecture', type: 'default' },
      ]
    },
    {
      panelName: 'Metrics',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'CPU (Capacity)', name: 'cpu_capacity', type: 'cpu' },
        { labelName: 'CPU (Request)', name: 'cpu_request', type: 'cpu' },
        { labelName: 'CPU (Limit)', name: 'cpu_limit', type: 'cpu' },
        { labelName: 'CPU (Usage)', name: 'cpu_usage', type: 'cpu' },
        { labelName: 'Memory (Capacity)', name: 'memory_capacity', type: 'memory' },
        { labelName: 'Memory (Request)', name: 'memory_request', type: 'memory' },
        { labelName: 'Memory (Limit)', name: 'memory_limit', type: 'memory' },
        { labelName: 'Memory (Usage)', name: 'memory_usage', type: 'memory' },
        { labelName: 'Pods (Capacity)', name: 'pods_capacity', type: 'number' },
        { labelName: 'Pods (Allocation)', name: 'pods_allocation', type: 'number' },
      ]
    },
    {
      panelName: 'Costs',
      tableRecordList: [],
      keyValueRecords: [],
    },
    {
      panelName: 'Conditions',
      tableRecordList: [],
      keyValueRecords: [],
    },
    {
      panelName: 'Pods',
      tableRecordList: [],
      keyValueRecords: [],
    },
    {
      panelName: 'Detail',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Detail', name: 'detail', type: 'default' },
      ]
    },
    {
      panelName: 'Other',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud Service Provider ID', name: 'cloud_context', type: 'default' },
      ]
    },
  ]
};

export default K8S_NODE_TEMPLATE;
