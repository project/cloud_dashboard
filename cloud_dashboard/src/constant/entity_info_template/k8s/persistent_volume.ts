import EntityInfoTemplate from 'model/EntityInfoTemplate';

// Template for displaying detailed information about entities in K8s.
const K8S_PERSISTENT_VOLUME_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'k8s',
  entityName: 'persistent_volume',
  entityRecords: [
    {
      panelName: 'Persistent Volume',
      tableRecordList: ['labels', 'annotations'],
      keyValueRecords: [
        { labelName: 'Name', name: 'name', type: 'default' },
        { labelName: 'Created', name: 'created', type: 'datetime' },
        { labelName: 'Labels', name: 'labels', type: 'default' },
        { labelName: 'Annotations', name: 'annotations', type: 'default' },
        { labelName: 'Capacity', name: 'capacity', type: 'default' },
        { labelName: 'Access Modes', name: 'access_modes', type: 'default' },
        { labelName: 'Reclaim Policy', name: 'reclaim_policy', type: 'default' },
        { labelName: 'Storage Class Name', name: 'storage_class_name', type: 'default' },
        { labelName: 'Claim', name: 'claim_ref', type: 'default' },
      ]
    },
    {
      panelName: 'Status',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Phase', name: 'phase', type: 'default' },
        { labelName: 'Reason', name: 'reason', type: 'default' },
      ]
    },
    {
      panelName: 'Detail',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Detail', name: 'detail', type: 'default' },
      ]
    },
    {
      panelName: 'Other',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud Service Provider ID', name: 'cloud_context', type: 'default' },
      ]
    },
  ]
};

export default K8S_PERSISTENT_VOLUME_TEMPLATE;
