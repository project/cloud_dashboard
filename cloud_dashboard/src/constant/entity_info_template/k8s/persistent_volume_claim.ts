import EntityInfoTemplate from 'model/EntityInfoTemplate';

// Template for displaying detailed information about entities in K8s.
const K8S_PERSISTENT_VOLUME_CLAIM_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'k8s',
  entityName: 'persistent_volume_claim',
  entityRecords: [
    {
      panelName: 'Persistent Volume Claim',
      tableRecordList: ['labels', 'annotations'],
      keyValueRecords: [
        { labelName: 'Name', name: 'name', type: 'default' },
        { labelName: 'Namespace', name: 'namespace', type: 'default' },
        { labelName: 'Phase', name: 'phase', type: 'default' },
        { labelName: 'VolumeName', name: 'volume_name', type: 'default' },
        { labelName: 'Capacity', name: 'capacity', type: 'default' },
        { labelName: 'Request', name: 'request', type: 'default' },
        { labelName: 'AccessMode', name: 'access_mode', type: 'default' },
        { labelName: 'StorageClass', name: 'storage_class', type: 'default' },
        { labelName: 'Created', name: 'created', type: 'datetime' },
        { labelName: 'Labels', name: 'labels', type: 'default' },
        { labelName: 'Annotations', name: 'annotations', type: 'default' },
      ]
    },
    {
      panelName: 'Detail',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Detail', name: 'detail', type: 'default' },
      ]
    },
    {
      panelName: 'Other',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud Service Provider ID', name: 'cloud_context', type: 'default' },
      ]
    },
  ]
};

export default K8S_PERSISTENT_VOLUME_CLAIM_TEMPLATE;
