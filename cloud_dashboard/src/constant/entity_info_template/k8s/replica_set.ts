import EntityInfoTemplate from 'model/EntityInfoTemplate';

const K8S_REPLICA_SET_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'k8s',
  entityName: 'replica_set',
  entityRecords: [
    {
      panelName: 'ReplicaSet',
      tableRecordList: ['labels', 'annotations'],
      keyValueRecords: [
        { labelName: 'Name', name: 'name', type: 'default' },
        { labelName: 'Labels', name: 'labels', type: 'default' },
        { labelName: 'Annotations', name: 'annotations', type: 'default' },
        { labelName: 'Namespace', name: 'namespace', type: 'default' },
        { labelName: 'Replicas', name: 'replicas', type: 'number' },
        { labelName: 'Selector', name: 'selector', type: 'default' },
        { labelName: 'Template used for pod', name: 'template', type: 'default' },
      ]
    },
    { 
      panelName: 'Status',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Available Replicas', name: 'available_replicas', type: 'number' },
        { labelName: 'Fully Labeled Replicas', name: 'fully_labeled_replicas', type: 'number' },
        { labelName: 'Conditions', name: 'conditions', type: 'number' },
        { labelName: 'Observed Generation', name: 'observed_generation', type: 'number' },
        { labelName: 'Ready Replicas', name: 'ready_replicas', type: 'number' },
      ]
    },
    {
      panelName: 'Detail',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Detail', name: 'detail', type: 'default' },
      ]
    },
    {
      panelName: 'Other',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud Service Provider ID', name: 'cloud_context', type: 'default' },
      ]
    },
  ]
}

export default K8S_REPLICA_SET_TEMPLATE;