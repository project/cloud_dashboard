import EntityInfoTemplate from 'model/EntityInfoTemplate';

// Template for displaying detailed information about entities in K8s.
const K8S_ROLE_BINDING_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'k8s',
  entityName: 'role_binding',
  entityRecords: [
    {
      panelName: 'Role Binding',
      tableRecordList: ['subjects', 'labels', 'annotations'],
      keyValueRecords: [
        { labelName: 'Name', name: 'name', type: 'default' },
        { labelName: 'Namespace', name: 'namespace', type: 'default' },
        { labelName: 'Subjects', name: 'subjects', type: 'default' },
        { labelName: 'Role', name: 'role_ref', type: 'default' },
        { labelName: 'Created', name: 'created', type: 'datetime' },
        { labelName: 'Labels', name: 'labels', type: 'default' },
        { labelName: 'Annotations', name: 'annotations', type: 'default' },
      ]
    },
    {
      panelName: 'Detail',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Detail', name: 'detail', type: 'default' },
      ]
    },
    {
      panelName: 'Other',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud Service Provider ID', name: 'cloud_context', type: 'default' },
      ]
    },
  ]
};

export default K8S_ROLE_BINDING_TEMPLATE;
