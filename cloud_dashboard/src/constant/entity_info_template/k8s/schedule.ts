import EntityInfoTemplate from 'model/EntityInfoTemplate';

const K8S_SCHEDULE_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'k8s',
  entityName: 'schedule',
  entityRecords: [
    {
      panelName: 'Schedule',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Kind', name: 'kind', type: 'default' },
        { labelName: 'Name', name: 'name', type: 'default' },
        { labelName: 'Namespace Name', name: 'namespace_name', type: 'default' },
        { labelName: 'Resource Name', name: 'resource_name', type: 'default' },
        { labelName: 'Start-up Time', name: 'start_time', type: 'default' },
        { labelName: 'Stop Time', name: 'stop_time', type: 'default' },
        { labelName: 'State', name: 'state', type: 'default' },
        { labelName: 'Launch template name', name: 'launch_template_name', type: 'default' },
      ]
    },
    {
      panelName: 'Detail',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Manifest', name: 'manifest', type: 'default' },
      ]
    },
    {
      panelName: 'Other',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud Service Provider ID', name: 'cloud_context', type: 'default' },
      ]
    },
  ]
}

export default K8S_SCHEDULE_TEMPLATE;
