import EntityInfoTemplate from 'model/EntityInfoTemplate';

const K8S_SECRET_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'k8s',
  entityName: 'secret',
  entityRecords: [
    {
      panelName: 'Secret',
      tableRecordList: ['annotations'],
      keyValueRecords: [
        { labelName: 'Name', name: 'name', type: 'default' },
        { labelName: 'Annotations', name: 'annotations', type: 'default' },
        { labelName: 'Namespace', name: 'namespace', type: 'default' },
        { labelName: 'Type', name: 'secret_type', type: 'default' },
      ]
    },
    {
      panelName: 'Data',
      tableRecordList: ['data'],
      keyValueRecords: [
        { labelName: 'Data', name: 'data', type: 'default' },
      ]
    },
    {
      panelName: 'Detail',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Detail', name: 'detail', type: 'default' },
      ]
    },
    {
      panelName: 'Other',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud Service Provider ID', name: 'cloud_context', type: 'default' },
      ]
    },
  ]
}

export default K8S_SECRET_TEMPLATE;
