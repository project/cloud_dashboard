import EntityInfoTemplate from 'model/EntityInfoTemplate';

const K8S_SERVICE_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'k8s',
  entityName: 'service',
  entityRecords: [
    {
      panelName: 'Service',
      tableRecordList: ['labels', 'annotations', 'selector'],
      keyValueRecords: [
        { labelName: 'Name', name: 'name', type: 'default' },
        { labelName: 'Labels', name: 'labels', type: 'default' },
        { labelName: 'Annotations', name: 'annotations', type: 'default' },
        { labelName: 'Namespace', name: 'namespace', type: 'default' },
        { labelName: 'Selector', name: 'selector', type: 'default' },
        // Field with `type` not supported by JSON API https://www.drupal.org/project/jsonapi/issues/2977669.
        // The JSON API renames it k8s_service_type.
        { labelName: 'Type', name: 'k8s_service_type', type: 'default' },
        { labelName: 'Session Affinity', name: 'session_affinity', type: 'default' },
        { labelName: 'Cluster IP', name: 'cluster_ip', type: 'default' },
        { labelName: 'Internal Endpoints', name: 'internal_endpoints', type: 'default' },
        { labelName: 'External Endpoints', name: 'external_endpoints', type: 'default' },
      ]
    },
    {
      panelName: 'Detail',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Detail', name: 'detail', type: 'default' },
      ]
    },
    {
      panelName: 'Other',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud Service Provider ID', name: 'cloud_context', type: 'default' },
      ]
    },
  ]
}

export default K8S_SERVICE_TEMPLATE;
