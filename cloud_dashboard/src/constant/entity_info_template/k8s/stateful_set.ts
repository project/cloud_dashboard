import EntityInfoTemplate from 'model/EntityInfoTemplate';

const K8S_STATEFUL_SET_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'k8s',
  entityName: 'stateful_set',
  entityRecords: [
    {
      panelName: 'StatefulSet',
      tableRecordList: ['labels', 'annotations'],
      keyValueRecords: [
        { labelName: 'Name', name: 'name', type: 'default' },
        { labelName: 'Namespace', name: 'namespace', type: 'default' },
        { labelName: 'Pod Management Policy', name: 'pod_management_policy', type: 'number' },
        { labelName: 'Service Name', name: 'service_name', type: 'number' },
        { labelName: 'Revision History Limit', name: 'revision_history_limit', type: 'number' },
        { labelName: 'Created', name: 'created', type: 'datetime' },
        { labelName: 'Labels', name: 'labels', type: 'default' },
        { labelName: 'Annotations', name: 'annotations', type: 'default' },
      ]
    },
    {
      panelName: 'Status',
      tableRecordList: ['data'],
      keyValueRecords: [
        { labelName: 'Collision Count', name: 'observed_generation', type: 'number' },
        { labelName: 'Replicas', name: 'replicas', type: 'number' },
        { labelName: 'Ready Replicas', name: 'ready_replicas', type: 'number' },
        { labelName: 'Current Replicas', name: 'current_replicas', type: 'number' },
        { labelName: 'Updated Replicas', name: 'updated_replicas', type: 'number' },
        { labelName: 'Current Revision', name: 'current_revision', type: 'default' },
        { labelName: 'Updated Revision', name: 'update_revision', type: 'default' },
        { labelName: 'Collision Count', name: 'collision_count', type: 'number' },
      ]
    },
    {
      panelName: 'Detail',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Detail', name: 'detail', type: 'default' },
      ]
    },
    {
      panelName: 'Other',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud Service Provider ID', name: 'cloud_context', type: 'default' },
      ]
    },
  ]
}

export default K8S_STATEFUL_SET_TEMPLATE;
