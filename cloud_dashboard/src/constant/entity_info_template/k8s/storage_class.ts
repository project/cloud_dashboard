import EntityInfoTemplate from 'model/EntityInfoTemplate';

const K8S_STORAGE_CLASS_TEMPLATE: EntityInfoTemplate = {
  cloudServiceProvider: 'k8s',
  entityName: 'storage_class',
  entityRecords: [
    {
      panelName: 'Storage Class',
      tableRecordList: ['labels', 'annotations', 'parameters'],
      keyValueRecords: [
        { labelName: 'Name', name: 'name', type: 'default' },
        { labelName: 'Created', name: 'created', type: 'datetime' },
        { labelName: 'Labels', name: 'labels', type: 'default' },
        { labelName: 'Annotations', name: 'annotations', type: 'default' },
        { labelName: 'Parameters', name: 'parameters', type: 'default' },
        { labelName: 'Provisioner', name: 'provisioner', type: 'default' },
        { labelName: 'Reclaim Policy', name: 'reclaim_policy', type: 'default' },
        { labelName: 'Volume Binding Mode', name: 'volume_binding_mode', type: 'default' },
      ]
    },
    {
      panelName: 'Detail',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Detail', name: 'detail', type: 'default' },
      ]
    },
    {
      panelName: 'Other',
      tableRecordList: [],
      keyValueRecords: [
        { labelName: 'Cloud Service Provider ID', name: 'cloud_context', type: 'default' },
      ]
    },
  ]
}

export default K8S_STORAGE_CLASS_TEMPLATE;
