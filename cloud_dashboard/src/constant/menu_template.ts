import CloudServiceProvider from 'model/CloudServiceProvider';
import MenuTemplate from 'model/MenuTemplate';
import menu_template from 'constant/menu_template.json';

// Template for displaying a list of entities in AWS Cloud.
const AWS_MENU_LIST = menu_template[0] as MenuTemplate[];

// Template for displaying a list of entities in K8s.
const K8S_MENU_LIST = menu_template[1] as MenuTemplate[];

export const MENU_TEMPLATE_LIST = [...AWS_MENU_LIST, ...K8S_MENU_LIST];

export const getMenuTemplateList = (cloudServiceProvider: CloudServiceProvider) => {
  switch (cloudServiceProvider) {
    case 'aws_cloud':
      return AWS_MENU_LIST;
    case 'k8s':
      return K8S_MENU_LIST;
    default:
      throw new Error('It is an unknown Cloud Context.');
  }
}
