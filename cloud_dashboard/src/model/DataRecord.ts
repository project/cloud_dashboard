interface DataRecord {
  id: string,
  entityTypeId: string,
  value: { [key: string]: string }
};

export default DataRecord;
