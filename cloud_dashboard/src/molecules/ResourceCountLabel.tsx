import Form from 'bootstrap3-components/Form';
import useDrupalJsonApi, { GetJsonDataType } from 'hooks/drupal_jsonapi';
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

/**
 * Get entity item's count.
 *
 * @param cloudContext Cloud context.
 * @param entityTypeId Entity type ID.
 * @param namespace Value of namespace.
 * @param namespaceName Value of namespace.
 * @returns Entity item's count.
 */
const getItemCount = async (
  getJsonData: GetJsonDataType,
  bundleId: string
) => {

  // Create URL for REST API.
  const url = `/cloud_dashboard/${bundleId}/entity/count`;

  // Download data.
  return (await getJsonData<{ count: number }>(url)).count;

};

/**
 * Label of resource item's count.
 * @param bundleId Bundle type ID.
 * @param itemCount resource item's count.
 * @param setItemCount Setter of itemCount.
 */
const ResourceCountLabel = ({ bundleId, itemCount, setItemCount }: {
  bundleId: string,
  itemCount: number,
  setItemCount: (n: number) => void
}) => {

  const { getJsonData } = useDrupalJsonApi();
  const { t } = useTranslation();

  // Set entity item's count.
  useEffect(() => {
    getItemCount(getJsonData, bundleId).then((count) => {
      setItemCount(count);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return <Form.Label>{t('ItemCount')}: {itemCount}</Form.Label>;

};

export default ResourceCountLabel;
