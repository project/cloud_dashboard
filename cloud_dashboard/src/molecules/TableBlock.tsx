import Table from 'bootstrap3-components/Table';
import { UPPERCASE_WORD_SET } from 'constant/entity_info_template/entity_info_template';
import EntityInfoRecordData from 'model/EntityInfoRecordData';
import React from 'react';

/**
 * Block of table view.
 *
 * @param record Record data fo table.
 */
const TableBlock = ({ record }: { record: EntityInfoRecordData }) => {

  if (record.type !== 'table' || record.record.length === 0) {
    return <></>;
  }

  const keys = Object.keys(record.record[0]);

  const convert = (keyName: string) => {
    switch (keyName) {
      case 'item_key':
        return 'Key';
      case 'item_value':
        return 'Value';
      default: {
        return keyName.split(('_')).map((word, index) => {
          if (UPPERCASE_WORD_SET.has((word))) {
            return word.toUpperCase();
          }
          if (index === 0) {
            return word.slice(0, 1).toUpperCase() + word.slice((1));
          }
          return word;
        }).join(' ');
      }
    }
  }

  return <div className="field field--name-tags field--type-key-value field--label-above">
    <div className="field--label">{record.title}</div>
    <div className="field--items">
      <div className="field--item">
        <Table hover={true} striped={true} responsive={true}>
          <thead>
            <tr>
              {
                keys.map((key, index) => {
                  return <th key={index}>{convert(key)}</th>;
                })
              }
            </tr>
          </thead>
          <tbody>
            {
              record.record.map((r, index) => {
                return <tr className={index % 2 === 0 ? 'odd' : 'even'}>
                  {
                    keys.map((key, index2) => {
                      return <td className="word-break-all">{
                        r[key].includes('\n')
                          ? <pre>{r[key]}</pre>
                          : <>{r[key]}</>
                      }</td>;
                    })
                  }
                </tr>;
              })
            }
          </tbody>
        </Table>
      </div>
    </div>
  </div>;

}

export default TableBlock;
