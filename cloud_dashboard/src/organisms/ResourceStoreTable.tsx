import { ITEMS_PER_PAGE } from 'constant/other';
import useDrupalJsonApi from 'hooks/drupal_jsonapi';
import DataColumn from 'model/DataColumn';
import DataRecord from 'model/DataRecord';
import ResourceStoreTemplate from 'model/ResourceStoreTemplate';
import SortInfo from 'model/SortInfo';
import DataTable from 'organisms/DataTable';
import React, { useContext, useEffect, useState } from 'react';
import { AppContext } from 'service/state';
import { convertEntityData } from 'service/utility';

/**
 * ListView of data.
 *
 * @returns JSX of ResourceTable.
 */
const ResourceStoreTable = ({ template, pageIndex }: {
  template: ResourceStoreTemplate,
  pageIndex: number,
}) => {

  const { cloudContextList } = useContext(AppContext);
  const { getEntityList } = useDrupalJsonApi();
  const [dataColumnList, setDataColumnList] = useState<DataColumn[]>([]);
  const [dataRecordList, setDataRecordList] = useState<DataRecord[]>([]);
  const [sortInfo, setSortInfo] = useState<SortInfo>({
    key: '', direction: 'ASC'
  });

  /** Update dataColumnList and dataRecordList */
  useEffect(() => {
    const init = async () => {
      // Load launch template's column data.
      const columnList = template.column;
      let newDataColumnList: DataColumn[] = columnList.map((column) => {
        return { key: column.name, label: column.labelName };
      });
      setDataColumnList(newDataColumnList);

      // Create function parameter.
      const parameter = {
        limit: ITEMS_PER_PAGE,
        offset: pageIndex * ITEMS_PER_PAGE,
        filter: {},
        sort: sortInfo
      };

      // Load launch template's data.
      const rawData = await getEntityList('cloud_store', parameter, template.bundleId);
      setDataRecordList(convertEntityData(template.bundleId, rawData, columnList, cloudContextList, {}));
    };
    init();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [cloudContextList, sortInfo, pageIndex]);

  return <DataTable dataColumnList={dataColumnList} dataRecordList={dataRecordList} sortInfo={sortInfo} setSortInfo={setSortInfo} hasOperationLinks={true} />;
}

export default ResourceStoreTable;
