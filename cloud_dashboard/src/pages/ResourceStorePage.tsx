import Col from 'bootstrap3-components/Col';
import FluidContainer from 'bootstrap3-components/FluidContainer';
import Form from 'bootstrap3-components/Form';
import Row from 'bootstrap3-components/Row';
import ResourceStoreTemplate from 'model/ResourceStoreTemplate';
import PageSelector from 'molecules/PageSelector';
import ResourceCountLabel from 'molecules/ResourceCountLabel';
import MenuBar from 'organisms/MenuBar';
import ResourceStoreTable from 'organisms/ResourceStoreTable';
import React, { useState } from 'react';

/**
 * Page of Cloud Resource.
 */
const ResourceStorePage = ({ template }: { template: ResourceStoreTemplate }) => {

  const [itemCount, setItemCount] = useState(0);
  const [pageIndex, setPageIndex] = useState(0);

  return <>
    <MenuBar />
    <FluidContainer className="px-0">
      <Row className="mx-0">
        <Col>
          <Form>
            <Form.Group style={{ marginTop: '2rem' }}>
              <ResourceCountLabel
                bundleId={template.bundleId}
                itemCount={itemCount}
                setItemCount={setItemCount} />
            </Form.Group>
          </Form>
        </Col>
      </Row>
    </FluidContainer>
    <PageSelector
      pageIndex={pageIndex}
      setPageIndex={setPageIndex}
      itemCount={itemCount} />
    <FluidContainer className="px-0">
      <Row className="mx-0">
        <Col>
          <Form>
            <ResourceStoreTable template={template} pageIndex={pageIndex} />
          </Form>
        </Col>
      </Row>
    </FluidContainer>
    <PageSelector
      pageIndex={pageIndex}
      setPageIndex={setPageIndex}
      itemCount={itemCount} />
  </>;

}

export default ResourceStorePage;
